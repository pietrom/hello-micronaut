package com.example.hello.micronaut.fake

import com.example.hello.micronaut.core.MessageSource

class HardcodedMessageSource : MessageSource {
    override fun getMessage(): String {
        return "Hello, World!"
    }
}