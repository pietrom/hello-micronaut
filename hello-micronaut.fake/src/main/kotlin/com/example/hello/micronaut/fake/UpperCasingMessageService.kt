package com.example.hello.micronaut.fake

import com.example.hello.micronaut.core.MessageService
import com.example.hello.micronaut.core.MessageSource

class UpperCasingMessageService(private val source: MessageSource) : MessageService {
    override fun prepareMessage(): String {
        return source.getMessage().uppercase()
    }
}