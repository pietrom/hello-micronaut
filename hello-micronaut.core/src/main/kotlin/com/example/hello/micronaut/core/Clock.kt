package com.example.hello.micronaut.core

import java.time.ZonedDateTime

interface Clock {
    val now : ZonedDateTime
}