package com.example.hello.micronaut.core

interface MessageService {
    fun prepareMessage() : String
}