package com.example.hello.micronaut.core

interface MessageSource {
    fun getMessage() : String
}
