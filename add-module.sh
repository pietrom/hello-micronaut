#!/bin/bash
prefix="hello-micronaut"

first=$(echo "$1" | cut -d '.' -f 1)
if [ "$first" == "$prefix" ]; then
  module="$1"
else
  module="$prefix.$1"
fi

moduleName=$(echo "$module" | sed s/$prefix\.//)

package="com.example.hello.micronaut.$moduleName"
packageDir=$(echo "$package" | tr '.' '/')

mkdir "$module"
mkdir -p "$module"/src/main/kotlin/"$packageDir"
mkdir -p "$module"/src/test/kotlin/"$packageDir"

include="include \"$module\""
grep "$include" settings.gradle > /dev/null
if [ $? -ne 0 ] ; then
  echo "$include" >> settings.gradle
fi

echo "package $package
import org.hamcrest.CoreMatchers.\`is\`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class SampleTest {
  @Test
  fun sample() {
    assertThat(Sample().text, \`is\`(\"sample\"))
  }
}
" > "$module"/src/test/kotlin/"$packageDir"/SampleTest.kt

echo "package $package

class Sample {
  val text: String = \"sample\"
}
" > "$module"/src/main/kotlin/"$packageDir"/Sample.kt

touch "$module/build.gradle"
