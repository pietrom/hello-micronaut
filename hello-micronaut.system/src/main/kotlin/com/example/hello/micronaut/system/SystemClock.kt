package com.example.hello.micronaut.system

import com.example.hello.micronaut.core.Clock
import java.time.ZonedDateTime

class SystemClock : Clock {
    public override val now: ZonedDateTime
        get() = ZonedDateTime.now()
}