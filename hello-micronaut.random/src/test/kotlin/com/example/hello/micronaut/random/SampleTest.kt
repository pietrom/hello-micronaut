package com.example.hello.micronaut.random
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class SampleTest {
  @Test
  fun sample() {
    assertThat(Sample().text, `is`("sample"))
  }
}

