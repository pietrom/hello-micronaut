package com.example

import com.example.hello.micronaut.core.Clock
import com.example.hello.micronaut.core.MessageService
import com.example.hello.micronaut.core.MessageSource
import com.example.hello.micronaut.fake.HardcodedMessageSource
import com.example.hello.micronaut.fake.UpperCasingMessageService
import com.example.hello.micronaut.system.SystemClock
import io.micronaut.context.annotation.Factory
import jakarta.inject.Singleton

@Factory
internal class BeanFactory {
    @Singleton
    fun messageSource(): MessageSource {
        return HardcodedMessageSource()
    }

    @Singleton
    fun messageService(source: MessageSource): MessageService {
        return UpperCasingMessageService(source)
    }

    @Singleton
    fun clock() : Clock = SystemClock()
}