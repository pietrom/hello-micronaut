package com.example

import com.example.hello.micronaut.core.Clock
import com.example.hello.micronaut.core.MessageService
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import java.time.ZonedDateTime
import java.util.*

@Controller("/hello")
class HelloController(private val clock : Clock, private val source: MessageService) {
    @Get("/v1")
    fun index(): HelloResponse {
        return HelloResponse(clock.now, source.prepareMessage())
    }
}

@Controller("/hello")
class HelloController2(private val clock : Clock, private val source: MessageService) {
    @Get("/v2")
    fun index(): HelloResponse {
        return HelloResponse(clock.now, source.prepareMessage() + " [2]")
    }
}

data class HelloResponse(val `when`: ZonedDateTime, val text: String)
